FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y nginx
ADD index.html /usr/share/nginx/html/index.html
EXPOSE 80/tcp
CMD ["nginx","-g daemon off;"]
